package com.zuitt;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner scannerName = new Scanner(System.in);

        System.out.println("First Name:");
        String fName = scannerName.nextLine();

        System.out.println("Last Name:");
        String lName = scannerName.nextLine();

        System.out.println("First Subject Grade:");
        Double subjectGrade1 = scannerName.nextDouble();
        System.out.println("Second Subject Grade::");
        Double subjectGrade2 = scannerName.nextDouble();
        System.out.println("Third Subject Grade::");
        Double subjectGrade3 = scannerName.nextDouble();

        Double averageGrade = (subjectGrade1 + subjectGrade2 + subjectGrade3) / 3;
        int avgGrade = averageGrade.intValue();
        String outputLineName = String.format("Good day, %s %s", fName, lName);

        System.out.println(outputLineName);
        System.out.println("Your grade average is: " + avgGrade);

    }
}
